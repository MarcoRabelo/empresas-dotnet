﻿namespace IMDbAPI.Application.ViewModels
{
    public class FilmViewModel
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public string Genre { get; set; }

        public string Synopsis { get; set; }

        public string Cast { get; set; }

        public string Director { get; set; }
    }
}
