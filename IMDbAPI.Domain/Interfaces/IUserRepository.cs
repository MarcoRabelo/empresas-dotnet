﻿using IMDbAPI.Domain.Entities;
using IMDbAPI.Domain.Models;
using System.Collections.Generic;

namespace IMDbAPI.Domain.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        (IEnumerable<User> users, PageInfo pageInfo) GetUsers(int pageNumber = 1, int pageSize = 20);
    }
}
