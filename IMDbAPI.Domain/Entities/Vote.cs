﻿namespace IMDbAPI.Domain.Entities
{
    public class Vote
    {
        public User User { get; set; }

        public Film Film { get; set; }

        public int Score { get; set; }
    }
}
