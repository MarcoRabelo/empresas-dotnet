﻿using IMDbAPI.Domain.Models;

namespace IMDbAPI.Domain.Entities
{
    public class User : BaseEntity
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public bool Admin { get; set; }

        public string Password { get; set; }
    }
}
