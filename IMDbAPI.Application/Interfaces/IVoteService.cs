﻿using IMDbAPI.Application.ViewModels;
using System.Collections.Generic;

namespace IMDbAPI.Application.Interfaces
{
    public interface IVoteService
    {
        IEnumerable<VoteViewModel> Get();
    }
}
