﻿using IMDbAPI.Application.ViewModels;
using IMDbAPI.Application.Wrappers;
using System.Collections.Generic;

namespace IMDbAPI.Application.Interfaces
{
    public interface IUserService
    {
        PagedResponse<List<UserViewModel>> GetUsers(int pageNumber, int pageSize);

        UserViewModel GetUserById(long Id);

        void Insert(UserViewModel userViewModel);

        void Update(UserViewModel userViewModel);

        bool Delete(long Id);

        UserAuthenticateResponseViewModel Authenticate(UserAuthenticateRequestViewModel userAuthenticate);
    }
}
