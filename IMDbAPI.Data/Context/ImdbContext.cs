﻿using IMDbAPI.Data.Extensions;
using IMDbAPI.Data.Mappings;
using IMDbAPI.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace IMDbAPI.Data.Context
{
    public class ImdbContext : DbContext
    {
        public ImdbContext(DbContextOptions<ImdbContext> options)
            : base(options) { }

        #region [ DBSets ]

        public DbSet<User> Users { get; set; }
        public DbSet<Film> Films { get; set; }
        public DbSet<Vote> Votes { get; set; }

        #endregion [ DBSets ]

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserMap());
            modelBuilder.ApplyConfiguration(new FilmMap());
            modelBuilder.ApplyConfiguration(new VoteMap());
            modelBuilder.SetDefaultValuesOnInsert();
            modelBuilder.SeedData();

            base.OnModelCreating(modelBuilder);
        }
    }
}
