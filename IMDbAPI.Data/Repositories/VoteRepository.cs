﻿using IMDbAPI.Data.Context;
using IMDbAPI.Domain.Entities;
using IMDbAPI.Domain.Interfaces;
using System.Collections.Generic;

namespace IMDbAPI.Data.Repositories
{
    public class VoteRepository : Repository<Vote>, IVoteRepository
    {
        public VoteRepository(ImdbContext context) : base(context) { }

        public IEnumerable<Vote> GetAll()
        {
            throw new System.NotImplementedException();
        }
    }
}
