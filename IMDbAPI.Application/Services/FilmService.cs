﻿using AutoMapper;
using IMDbAPI.Application.Interfaces;
using IMDbAPI.Application.ViewModels;
using IMDbAPI.Application.Wrappers;
using IMDbAPI.Domain.Entities;
using IMDbAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace IMDbAPI.Application.Services
{
    public class FilmService : IFilmService
    {
        private readonly IFilmRepository _filmRepository;
        private readonly IMapper _mapper;

        public FilmService(IFilmRepository filmRepository, IMapper mapper)
        {
            _filmRepository = filmRepository;
            _mapper = mapper;
        }

        public IEnumerable<FilmViewModel> Get()
        {
            return _mapper.Map<List<FilmViewModel>>(_filmRepository.GetAll());
        }

        public Response<FilmViewModel> GetById(long Id)
        {
            Film film = Get(Id);

            return new Response<FilmViewModel>(_mapper.Map<FilmViewModel>(film));
        }

        public void Insert(FilmViewModel filmViewModel)
        {
            if (filmViewModel.Id > 0)
            {
                throw new Exception("O Id do filme precisa ser 0 (zero) para que seja possível efetuar a inclusão.");
            }
            _filmRepository.Create(_mapper.Map<Film>(filmViewModel));
        }

        public void Update(FilmViewModel filmViewModel)
        {
            Get(filmViewModel.Id);
            Film film = _mapper.Map<Film>(filmViewModel);
            film.Active = true;
            _filmRepository.Update(film);
        }

        private Film Get(long Id)
        {
            Film film = _filmRepository.Find(a => a.Id == Id && a.Active);

            if (film == null)
            {
                throw new Exception("Filme não encontrado!");
            }

            return film;
        }
    }
}
