﻿using IMDbAPI.Application.ViewModels;
using IMDbAPI.Application.Wrappers;
using System.Collections.Generic;

namespace IMDbAPI.Application.Interfaces
{
    public interface IFilmService
    {
        IEnumerable<FilmViewModel> Get();

        Response<FilmViewModel> GetById(long Id);

        void Insert(FilmViewModel filmViewModel);

        void Update(FilmViewModel filmViewModel);
    }
}
