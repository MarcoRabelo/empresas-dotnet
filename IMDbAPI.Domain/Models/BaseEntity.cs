﻿namespace IMDbAPI.Domain.Models
{
    public abstract class BaseEntity
    {
        public long Id { get; set; }

        public bool Active { get; set; }
    }
}
