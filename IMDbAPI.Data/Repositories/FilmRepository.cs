﻿using IMDbAPI.Data.Context;
using IMDbAPI.Domain.Entities;
using IMDbAPI.Domain.Interfaces;
using System.Collections.Generic;

namespace IMDbAPI.Data.Repositories
{
    public class FilmRepository : Repository<Film>, IFilmRepository
    {
        public FilmRepository(ImdbContext context) : base(context) { }

        public IEnumerable<Film> GetAll()
        {
            return Query(a => a.Active);
        }
    }
}
