﻿using AutoMapper;
using IMDbAPI.Application.ViewModels;
using IMDbAPI.Domain.Entities;

namespace IMDbAPI.Application.AutoMapper
{
    public class AutoMapperConfiguration : Profile
    {
        public AutoMapperConfiguration()
        {
            #region [ ViewModel -> Domain ]

            CreateMap<UserViewModel, User>();
            CreateMap<FilmViewModel, Film>();
            CreateMap<VoteViewModel, Vote>();

            #endregion [ ViewModel -> Domain ]

            #region [ Domain -> ViewModel ]

            CreateMap<User, UserViewModel>();
            CreateMap<Film, FilmViewModel>();
            CreateMap<Vote, VoteViewModel>();

            #endregion [ Domain -> ViewModel ]
        }
    }
}
