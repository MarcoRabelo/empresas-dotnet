﻿using IMDbAPI.Domain.Entities;
using System.Collections.Generic;

namespace IMDbAPI.Domain.Interfaces
{
    public interface IVoteRepository : IRepository<Vote>
    {
        IEnumerable<Vote> GetAll();
    }
}
