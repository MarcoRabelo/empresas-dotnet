﻿using IMDbAPI.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IMDbAPI.Data.Mappings
{
    public class FilmMap : IEntityTypeConfiguration<Film>
    {
        public void Configure(EntityTypeBuilder<Film> builder)
        {
            builder.HasKey(a => a.Id);
            builder.Property(a => a.Active).IsRequired();
            builder.Property(a => a.Cast).HasMaxLength(200).IsRequired();
            builder.Property(a => a.Director).HasMaxLength(100).IsRequired();
            builder.Property(a => a.Genre).HasMaxLength(100).IsRequired();
            builder.Property(a => a.Synopsis).HasMaxLength(500).IsRequired();
            builder.Property(a => a.Title).HasMaxLength(150).IsRequired();
        }
    }
}
