﻿namespace IMDbAPI.Application.ViewModels
{
    public class VoteViewModel
    {
        public UserViewModel User { get; set; }

        public FilmViewModel Film { get; set; }

        public int Score { get; set; }
    }
}
