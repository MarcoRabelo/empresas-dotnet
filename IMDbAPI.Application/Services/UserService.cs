﻿using AutoMapper;
using IMDbAPI.Application.Interfaces;
using IMDbAPI.Application.ViewModels;
using IMDbAPI.Application.Wrappers;
using IMDbAPI.Auth.Services;
using IMDbAPI.Domain.Entities;
using IMDbAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace IMDbAPI.Application.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public bool Delete(long Id)
        {
            return _userRepository.Delete(Get(Id));
        }

        public PagedResponse<List<UserViewModel>> GetUsers(int pageNumber, int pageSize)
        {
            var pagedUsers = _userRepository.GetUsers(pageNumber, pageSize);
            return new PagedResponse<List<UserViewModel>>(_mapper.Map<List<UserViewModel>>(pagedUsers.users), pagedUsers.pageInfo.CurrentPage, pagedUsers.pageInfo.PageSize);
        }

        public UserViewModel GetUserById(long Id)
        {
            User user = Get(Id);

            return _mapper.Map<UserViewModel>(user);
        }

        public void Insert(UserViewModel userViewModel)
        {
            if (userViewModel.Id > 0)
            {
                throw new Exception("O Id do usuário precisa ser 0 (zero) para que seja possível efetuar a inclusão.");
            }
            User user = _mapper.Map<User>(userViewModel);
            user.Password = ComputeMD5Hash(user.Password);
            _userRepository.Create(user);
        }

        public void Update(UserViewModel userViewModel)
        {
            User dbUser = Get(userViewModel.Id);

            User vmUser = _mapper.Map<User>(userViewModel);
            vmUser.Password = string.IsNullOrEmpty(vmUser.Password) ? dbUser.Password : ComputeMD5Hash(vmUser.Password);
            vmUser.Active = true;

            _userRepository.Update(vmUser);
        }

        public UserAuthenticateResponseViewModel Authenticate(UserAuthenticateRequestViewModel userAuthenticate)
        {
            if (string.IsNullOrEmpty(userAuthenticate.Email) || string.IsNullOrEmpty(userAuthenticate.Password))
            {
                throw new Exception("O e-mail e a senha são obrigatórios.");
            }

            userAuthenticate.Password = ComputeMD5Hash(userAuthenticate.Password);

            User user = _userRepository.Find(x => x.Active &&
                                             x.Email.ToLower().Equals(userAuthenticate.Email.ToLower()) &&
                                             x.Password.Equals(userAuthenticate.Password));
            if (user == null)
            {
                throw new Exception("E-mail e/ou senha inválidos.");
            }

            return new UserAuthenticateResponseViewModel(_mapper.Map<UserViewModel>(user), TokenService.GenerateToken(user));
        }

        private User Get(long Id)
        {
            User user = _userRepository.Find(a => a.Id == Id && a.Active);

            if (user == null)
            {
                throw new Exception("Usuário não encontrado!");
            }

            return user;
        }

        private string ComputeMD5Hash(string text)
        {
            //TODO: include in the appsettings
            string prefix = "7ed00d976a79428bb04c52d1672150e5";
            string suffix = "a7b80fe9c68f41438314a5d2a6b81571";

            using MD5 md5 = MD5.Create();
            byte[] retVal = md5.ComputeHash(Encoding.Unicode.GetBytes($"{prefix}-{text}-{suffix}"));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < retVal.Length; i++)
            {
                sb.Append(retVal[i].ToString("x2"));
            }
            return sb.ToString();
        }
    }
}
