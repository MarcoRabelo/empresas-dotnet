﻿using IMDbAPI.Domain.Models;

namespace IMDbAPI.Domain.Entities
{
    public class Film : BaseEntity
    {
        public string Title { get; set; }

        public string Genre { get; set; }

        public string Synopsis { get; set; }

        public string Cast { get; set; }

        public string Director { get; set; }
    }
}
