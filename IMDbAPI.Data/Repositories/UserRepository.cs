﻿using IMDbAPI.Data.Context;
using IMDbAPI.Domain.Entities;
using IMDbAPI.Domain.Interfaces;
using IMDbAPI.Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace IMDbAPI.Data.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ImdbContext context) : base(context) { }

        public (IEnumerable<User> users, PageInfo pageInfo) GetUsers(int pageNumber = 1, int pageSize = 20)
        {
            var results = PagedQuery(a => a.Active && !a.Admin, null, pageNumber, pageSize);

            return (results.results.AsEnumerable(), results.pageInfo);
        }
    }
}
