﻿using System;

namespace IMDbAPI.Application.Wrappers
{
    public class PagedResponse<TEntity> : Response<TEntity>
    {
        public int PageNumber { get; set; }

        public int PageSize { get; set; }

        public Uri FirstPage { get; set; }

        public Uri LastPage { get; set; }

        public int TotalPages { get; set; }

        public int TotalRecords { get; set; }

        public Uri NextPage { get; set; }

        public Uri PreviousPage { get; set; }

        public PagedResponse(TEntity data, int pageNumber, int pageSize)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            Data = data;
            Message = null;
            Succeeded = true;
            Errors = null;
        }
    }
}
