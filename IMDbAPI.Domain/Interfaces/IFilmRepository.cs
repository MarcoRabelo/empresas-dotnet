﻿using IMDbAPI.Domain.Entities;
using System.Collections.Generic;

namespace IMDbAPI.Domain.Interfaces
{
    public interface IFilmRepository : IRepository<Film>
    {
        IEnumerable<Film> GetAll();
    }
}
