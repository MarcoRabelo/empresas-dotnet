﻿using IMDbAPI.Application.Interfaces;
using IMDbAPI.Application.ViewModels;
using IMDbAPI.Auth.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Claims;

namespace IMDbAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController, Authorize]
    public class FilmsController : ControllerBase
    {
        private readonly IFilmService _filmService;

        public FilmsController(IFilmService filmService)
        {
            _filmService = filmService;
        }

        // GET: api/<FilmsController>
        [HttpGet, AllowAnonymous]
        public IActionResult Get()
        {
            return Ok(_filmService.Get());
        }

        // GET api/<FilmsController>/5
        [HttpGet("{id}"), AllowAnonymous]
        public IActionResult Get(long id)
        {
            try
            {
                return Ok(_filmService.GetById(id));
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // POST api/<FilmsController>
        [HttpPost]
        public IActionResult Post([FromBody] FilmViewModel filmViewModel)
        {
            try
            {
                if (Convert.ToBoolean(TokenService.GetValueFromClaim(HttpContext.User.Identity, ClaimTypes.Role)))
                {
                    _filmService.Insert(filmViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }

        // PUT api/<FilmsController>
        [HttpPut]
        public IActionResult Put([FromBody] FilmViewModel filmViewModel)
        {
            try
            {
                if (Convert.ToBoolean(TokenService.GetValueFromClaim(HttpContext.User.Identity, ClaimTypes.Role)))
                {
                    _filmService.Update(filmViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }
    }
}
