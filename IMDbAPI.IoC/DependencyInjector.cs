﻿using IMDbAPI.Application.Interfaces;
using IMDbAPI.Application.Services;
using IMDbAPI.Data.Repositories;
using IMDbAPI.Domain.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace IMDbAPI.IoC
{
    public static class DependencyInjector
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IFilmService, FilmService>()
                    .AddScoped<IUserService, UserService>()
                    .AddScoped<IVoteService, VoteService>();
        }

        public static void RegisterRepositories(IServiceCollection services)
        {
            services.AddScoped<IFilmRepository, FilmRepository>()
                    .AddScoped<IUserRepository, UserRepository>()
                    .AddScoped<IVoteRepository, VoteRepository>();
        }
    }
}
