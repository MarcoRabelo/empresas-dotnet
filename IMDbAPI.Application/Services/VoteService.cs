﻿using IMDbAPI.Application.Interfaces;
using IMDbAPI.Application.ViewModels;
using IMDbAPI.Domain.Interfaces;
using System.Collections.Generic;

namespace IMDbAPI.Application.Services
{
    public class VoteService : IVoteService
    {
        private readonly IVoteRepository _voteRepository;

        public VoteService(IVoteRepository voteRepository)
        {
            _voteRepository = voteRepository;
        }

        public IEnumerable<VoteViewModel> Get()
        {
            throw new System.NotImplementedException();
        }
    }
}
